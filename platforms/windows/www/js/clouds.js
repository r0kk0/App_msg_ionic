﻿var position = "center";
var lastPosition = "center";
var contentCSS = "";
var body = $("body");
var content = $(".content");
window.suspendAnimation = false;

var xMovement = 15;
var yMovement = 30;
var halfX = xMovement/2;
var halfY = yMovement/2;

window.ondeviceorientation = function(event) {
    var gamma = event.gamma/90;
    var beta = event.beta/180;

    var temp = 0;

    //fix for holding the device upside down
    if ( gamma >= 1 ) {
        gamma = 2- gamma;
    } else if ( gamma <= -1) {
        gamma = -2 - gamma;
    }

    // shift values/motion based upon device orientation
    switch (window.orientation) {
        case 90:
            temp = gamma;
            gamma = beta;
            beta = temp;
            break;
        case -90:
            temp = -gamma;
            gamma = beta;
            beta = temp;
            break;

    }

    // update positions to be used for CSS
    var yPosition = -yMovement - (beta * yMovement);
    var xPosition = -xMovement - (gamma * xMovement);
    var contentX = (-xMovement - xPosition)/2;
    var contentY = (-yMovement - yPosition)/2;

    // generate css styles
    position = xPosition.toFixed(1) + "px " + yPosition.toFixed(1) + "px";
    contentCSS = "translate3d( " + (contentX.toFixed(1)) + "px, " + (contentY.toFixed(1)) + "px, " + " 0px)";
}

function updateBackground() {

    if (!window.suspendAnimation) {
        if ( position.valueOf() != lastPosition.valueOf() ) {

            body.css( "background-position", position);
            content.css( "-webkit-transform", contentCSS);
            lastPosition = position;
        }
    } else {
        lastPosition = "translate3d(0px, 0px, 0px)";;
    }

    window.requestAnimationFrame(updateBackground);
}

window.requestAnimationFrame(updateBackground);
