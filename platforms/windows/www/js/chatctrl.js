﻿angular.module('mobionicApp.chatcontrollers', [])

    .controller('ChatLoginCtrl', function ($scope, $ionicModal, $state, $ionicLoading, $rootScope, $stateParams, $localstorage, auth, $http, alert) {


        $scope.user = {
            'email': $localstorage.get('login_user'),
            'password': $localstorage.get('login_password')
        };


        $scope.signin = function (user) {

            auth.login(user.email, user.password)
                .success(function (res) {

                    $localstorage.set('user_id', res.user_id);
                    $localstorage.set('login_user', user.email);
                    $localstorage.set('login_password', user.password);
                })
                .error(function (err) {
                    alert('warning', 'Attenzione', 'Utente o password errati!');
                });
        };

    })

    .controller('ChatCtrl', function ($scope, $rootScope, $ionicModal, $interval, $state, $ionicLoading, auth, $stateParams, $localstorage, Chats) {




        //var user_id = $localstorage.get('user_id');
        $scope.utente = $localstorage.get('user_id');
        //$scope.chats = chatordini.chatordini($localstorage.get('user_id'));

        $scope.initFirst = function () {
            ripeti = $interval(function () {
                Chats.all().success(function (cliente) {
                    $scope.conversation_id = cliente[0].id;
                    Chats.risposte(cliente[0].id).success(function (risposta) {
                        if (risposta.length > 0) {

                            $scope.chats = risposta;
                        }
                    });
                });
            }, 2000);
        };

        $scope.$on('$destroy', function () {

            $interval.cancel(ripeti);

        });

       /* $scope.chatordini = function(message, user_id, conversation_id){

            $scope.chats = chatordini.chatordini($localstorage.get('user_id'));

            $scope.chats.$add({
                "conversation_id": conversation_id,
                "reply": message,
                "letto": "0"
            });

        };*/

        $scope.sendMessage = function (message, user_id, conversation_id) {
            Chats.send(message, user_id, conversation_id).success(function () {

                if (angular.isDefined(ripeti)) {
                    $interval.cancel(ripeti);
                    ripeti = undefined;
                }
                formchat.reset();
                $scope.initFirst();
            });
        }


    })

    .directive('myUserid', function ($http, Chats, $localstorage) {
        return {
            replace: true,
            scope: {user: '@'},
            template: '<div>{{nome}}</div>',
            link: function (scope, element, attrs) {
                scope.$watch('user', function (data) {

                    scope.name = data == $localstorage.get('user_id') ? 'Io' : 'MSG Boccea';
                    /*Chats.trova_cliente(data).success(function (cliente) {
                     scope.nome = cliente.name;
                     });*/
                })
            }
        }
    })

    .filter('nome', function ($localstorage) {
        return function (input) {
            return input == $localstorage.get('user_id') ? 'Io' : 'MSG Boccea';
        }
    })

    .filter('split', function () {
        return function (input, splitChar, splitIndex) {

            return input.split(splitChar)[splitIndex];
        }
    })

    .filter('capitalize', function () {
        return function (input, scope) {
            if (input != null)
                input = input.toLowerCase();
            return input.substring(0, 1).toUpperCase() + input.substring(1);
        }
    });