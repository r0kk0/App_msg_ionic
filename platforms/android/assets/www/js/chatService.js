angular.module('mobionicApp.chatservice', [])


    /*.factory('chatordini', function ($firebaseArray) {

        return {

            chatordini: function (user_id) {

                var itemsRef = new Firebase("https://glaring-torch-3704.firebaseio.com/chatordini/" + user_id);
                return $firebaseArray(itemsRef);
            }

        }

    })*/

    .factory('Chats', function ($localstorage, $http, API_URL, $firebaseArray) {

        return {

            all: function () {

                return $http.get(API_URL + 'clienti/conversazione');
                /*.success(function(cliente){
                 return $http.get(API_URL + 'clienti/risposte/' + cliente.id);
                 });*/

            },


            risposte: function (conv_id) {
                return $http.get(API_URL + 'clienti/risposte/' + conv_id);
            },

            trova_cliente: function (cliente_id) {
                return $http.get(API_URL + 'clienti/' + cliente_id);
            },

            send: function (message, user_id, conversation_id) {
                if (message.length > 0) {
                    var chatMessage = {
                        user_id: user_id,
                        conversation_id: conversation_id,
                        reply: message,
                        letto: "0"
                    };
                    return $http({
                        method: 'POST',
                        url: API_URL + 'clienti/aggiorna_risposta',
                        //headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                        data: chatMessage
                    });
                }
            }

        }
    })
    .service('alert', function ($rootScope, $timeout) {
        var alertTimeout;
        return function (type, title, message, timeout) {
            $rootScope.alert = {
                hasBeenShown: true,
                show: true,
                type: type,
                message: message,
                title: title
            };
            $timeout.cancel(alertTimeout);
            alertTimeout = $timeout(function () {
                $rootScope.alert.show = false;

            }, timeout || 2000);
        };
    });
