angular.module('mobionicApp.authService', [])


    .factory('authInterceptor', function (authToken, $q, $injector) {

        return {
            request: function (config) {
                config.headers = config.headers || {};
                var token = authToken.getToken();

                if (token)
                    config.headers.Authorization = 'Bearer ' + token;

                return config;
            },
            response: function (response) {
                return response;
            },
            responseError: function(response){
                if (response.status === 401 || response.status === 403 || response.status === 500) {
                    var stateService = $injector.get('$state');
                    stateService.go('app.chat-login');
                }
                return $q.reject(response);
            }
        };

    })

    .factory('authToken', function ($window) {

        var storage = $window.localStorage;
        var cachedToken;
        var userToken = 'userToken';

        var authToken = {

            setToken: function (token) {
                cachedToken = token;
                storage.setItem(userToken, token);
            },
            getToken: function () {
                if (!cachedToken)
                    cachedToken = storage.getItem(userToken);

                return cachedToken;
            },
            isAuthenticated: function () {
                return !!authToken.getToken();
            },
            removeToken: function () {
                cachedToken = null;
                storage.removeItem(userToken);
            }

        };

        return authToken;
    })
    .service('auth', function auth($state, $http, authToken, API_URL) {

        function authSuccessful(res) {

            authToken.setToken(res.token);
            $state.go('app.chat-room');

        }


        this.login = function (email, password) {
            return $http.post(API_URL + 'signin', {
                email: email,
                password: password
            }).success(authSuccessful);
        };
    });