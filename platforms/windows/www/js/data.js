﻿angular.module('mobionicApp.data', [])

// Home Data: Home page configuration
    .factory('Data', function () {
        var data = {};

        data.items = [
            {
                title: 'Notizie dal sito',
                icon: 'ion-social-wordpress',
                note: 'News',
                url: '#/app/posts',
                badge: false,
                newsnumber: 0
            },
            /*       {
             title: 'News',
             icon: 'ion-ios7-calendar-outline',
             note: 'Latest News',
             url: '#/app/news'
             },
             {
             title: 'Products',
             icon: 'ion-ios7-cart',
             note: 'Our Products',
             url: '#/app/products'
             },*/
            {
                title: 'Galleria',
                icon: 'ion-images',
                note: 'Ultimi Arrivi',
                url: '#/app/gallery',
                badge: false,
                newsnumber: 0
            },
            {
                title: 'Dove siamo',
                icon: 'ion-map',
                note: 'Mappa',
                url: '#/app/map'
            },
            {
                title: 'Chi Siamo',
                icon: 'ion-person-stalker',
                note: 'Noi',
                url: '#/app/about'
            },
            {
                title: 'Contatti',
                icon: 'ion-email',
                note: 'Contattaci',
                url: '#/app/contact'
            },
            {
                title: 'Controlla il tuo residuo',
                icon: 'ion-bag',
                note: 'Residuo',
                url: '#/app/residuo'
            },
            {
                title: 'Notizie da AIC Lazio',
                icon: 'ion-social-rss',
                note: 'AIC RSS',
                url: '#/app/feeds-refresher'
            },
            {
                title: 'Linea Diretta con il Negozio',
                icon: 'ion-chatboxes',
                note: 'Chat',
                url: '#/app/chat-login'
            },
            {
                title: 'Notifiche & Promozioni',
                icon: 'ion-ios-lightbulb-outline',
                note: 'Notifiche',
                url: '#/app/notifiche'
            }
            /*        {
             title: 'Wordpress Pagination',
             icon: 'ion-ionic',
             note: 'Server Side',
             url: '#/app/serverposts'
             }*/
            // {
            //     title: 'Mobile Plugins',
            //     icon: 'ion-iphone',
            //     note: 'Cordova/PhoneGap',
            //     url: '#/app/plugins'
            // },
        ];

        return data;
    })

// Menu Data: Menu configuration
    .factory('MenuData', function () {
        var data = {};

        data.items = [
            {
                title: 'Home',
                icon: 'ion-ios7-home',
                url: '#/app'
            },
            {
                title: 'Notizie dal sito',
                icon: 'ion-social-wordpress',
                note: 'News',
                url: '#/app/posts'
            },
            {
                title: 'Galleria',
                icon: 'ion-images',
                note: 'Le Nostre Foto',
                url: '#/app/gallery'
            },
            {
                title: 'Dove siamo',
                icon: 'ion-map',
                note: 'Mappa',
                url: '#/app/map'
            },
            {
                title: 'Chi Siamo',
                icon: 'ion-person-stalker',
                note: 'Noi',
                url: '#/app/about'
            },
            {
                title: 'Contatti',
                icon: 'ion-email',
                note: 'Contattaci',
                url: '#/app/contact'
            },
            {
                title: 'Controlla il tuo residuo',
                icon: 'ion-bag',
                note: 'Residuo',
                url: '#/app/residuo'
            },
            {
                title: 'Notizie da AIC Lazio',
                icon: 'ion-social-rss',
                note: 'AIC RSS',
                url: '#/app/feeds-refresher'
            }
        ];

        return data;
    })

// Plugins Data: Mobile Plugins configuration
    .factory('PluginsData', function () {
        var data = {};

        data.items = [
            {
                title: 'Device',
                icon: 'ion-ipad',
                note: 'Device API',
                url: '#/app/plugins/device'
            },
            {
                title: 'Geolocation',
                icon: 'ion-location',
                note: 'Geolocation API',
                url: '#/app/plugins/geolocation'
            },
            {
                title: 'Notifications',
                icon: 'ion-alert',
                note: 'Dialogs API',
                url: '#/app/plugins/notifications'
            },
            {
                title: 'Barcode',
                icon: 'ion-qr-scanner',
                note: 'Barcode Scanner',
                url: '#/app/plugins/barcodescanner'
            }
        ];

        return data;
    })

// Map Data: Map configuration
    .factory('MapData', function () {
        var data = {};

        data.map = {
            zoom: 18,
            center: {
                latitude: 41.903463,
                longitude: 12.423923
            },
            markers: [
                {
                    id: 1,
                    icon: 'img/blue_marker.png',
                    latitude: 41.903463,
                    longitude: 12.423923,
                    title: 'Il Mondo Senza Glutine Boccea'
                }]
        };

        return data;
    })

// Gallery Data: Gallery configuration
    .factory('GalleryData', function () {
        var data = {};

        data.items = [
            {
                label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                src: 'img/negozio/IMG_0084.jpg',
                location: 'New York, June 2014'
            },
            {
                label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                src: 'img/negozio/IMG_0085.jpg',
                location: 'New York, June 2014'
            },
            {
                label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                src: 'img/negozio/IMG_0086.jpg',
                location: 'New York, June 2014'
            },
            {
                label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                src: 'img/negozio/IMG_0088.jpg',
                location: 'New York, June 2014'
            },
            {
                label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                src: 'img/negozio/IMG_0089.jpg',
                location: 'New York, June 2014'
            },
            {
                label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                src: 'img/negozio/IMG_0091.jpg',
                location: 'New York, June 2014'
            },
            {
                label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                src: 'img/negozio/IMG_0094.jpg',
                location: 'New York, June 2014'
            },
            {
                label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                src: 'img/negozio/IMG_0097.jpg',
                location: 'New York, June 2014'
            },
            {
                label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                src: 'img/negozio/IMG_0098.jpg',
                location: 'New York, June 2014'
            },
            {
                label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                src: 'img/negozio/IMG_0087m.jpg',
                location: 'New York, June 2014'
            },
            {
                label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                src: 'img/negozio/IMG_0090m.jpg',
                location: 'New York, June 2014'
            },
            {
                label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                src: 'img/negozio/IMG_0092m.jpg',
                location: 'New York, June 2014'
            },
            {
                label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                src: 'img/negozio/IMG_0093m.jpg',
                location: 'New York, June 2014'
            },
            {
                label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                src: 'img/negozio/IMG_0095.jpg',
                location: 'New York, June 2014'
            },
            {
                label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                src: 'img/negozio/IMG_0096.jpg',
                location: 'New York, June 2014'
            }
        ];

        return data;
    })

// News Data: JSON
    .factory('NewsData', function ($http, $q, NewsStorage) {

        var json = 'json/news.json';

        var deferred = $q.defer();
        var promise = deferred.promise;
        var data = [];
        var service = {};

        service.async = function () {
            $http({method: 'GET', url: json, timeout: 5000}).
                // this callback will be called asynchronously
                // when the response is available.
                success(function (d) {
                    data = d.result;
                    NewsStorage.save(data);
                    deferred.resolve();
                }).
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                error(function () {
                    data = NewsStorage.all();
                    deferred.reject();
                });

            return promise;

        };

        service.getAll = function () {
            return data;
        };

        service.get = function (newId) {
            return data[newId];
        };

        return service;
    })

// Products Data: JSON
    .factory('ProductsData', function ($http, $q, ProductsStorage) {

        var json = 'json/products.json';

        var deferred = $q.defer();
        var promise = deferred.promise;
        var data = [];
        var service = {};

        service.async = function () {
            $http({method: 'GET', url: json, timeout: 5000}).
                // this callback will be called asynchronously
                // when the response is available.
                success(function (d) {
                    data = d.result;
                    ProductsStorage.save(data);
                    deferred.resolve();
                }).
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                error(function () {
                    data = ProductsStorage.all();
                    deferred.reject();
                });

            return promise;

        };

        service.getAll = function () {
            return data;
        };

        service.get = function (productId) {
            return data[productId];
        };

        service.getLetterLimit = function () {
            return 100;
        };

        return service;
    })

// Gallery Data: Gallery configuration
    /*.factory('GalleryData', function(){
     var data = {};

     data.items = [
     {
     description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
     src: 'img/gallery-1.jpg',
     location: 'New York, June 2014'
     },
     {
     description: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco.',
     src: 'img/gallery-2.jpg',
     location: 'Athens, August 2013'
     },
     {
     description: 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
     src: 'img/gallery-3.jpg',
     location: 'Roma, May 2013'
     }
     ];

     return data;
     })*/

// About Data: JSON
    .factory('AboutData', function ($http, $q, AboutStorage) {

        var json = 'json/about.json';

        var deferred = $q.defer();
        var promise = deferred.promise;
        var data = [];
        var service = {};

        service.async = function () {
            $http({method: 'GET', url: json, timeout: 5000}).
                // this callback will be called asynchronously
                // when the response is available.
                success(function (d) {
                    data = d.result;
                    AboutStorage.save(data);
                    deferred.resolve();
                }).
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                error(function () {
                    data = AboutStorage.all();
                    deferred.reject();
                });

            return promise;

        };

        service.getAll = function () {
            return data;
        };

        service.get = function (memberId) {
            return data[memberId];
        };

        return service;
    })

    .factory('instaData', function ($http, $q) {

        var json = 'https://api.instagram.com/v1/tags/msgboccea/media/recent?client_id=16d983298df94287ac0198a56303a74e';


        var deferred = $q.defer();
        var promise = deferred.promise;
        var data = [];
        var service = {};

        service.async = function () {
            $http({method: 'GET', url: json, timeout: 5000}).
                // this callback will be called asynchronously
                // when the response is available.
                success(function (d) {
                    data = d;
                    //PostsStorage.save(data);
                    deferred.resolve();
                }).
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                error(function () {
                    //data = PostsStorage.all();
                    deferred.reject();
                });

            return promise;

        };

        service.getAll = function () {
            return data;
        };

        //service.get = function(postId) { return data.posts[postId]; };

        return service;
    })
// Posts Data: JSON Wordpress Posts configuration
    .factory('PostsData', function ($http, $q, PostsStorage) {

        /* (For DEMO purposes) Local JSON data */
        var json = 'http://www.mondosenzaglutineroma.it/api/get_recent_posts';

        /* Set your URL as you can see in the following example */
        // var json = 'YourWordpressURL/?json=get_recent_posts';

        /* With user-friendly permalinks configured */
        // var json = 'YourWordpressURL/api/get_recent_posts';

        var deferred = $q.defer();
        var promise = deferred.promise;
        var data = [];
        var service = {};

        service.async = function () {
            $http({method: 'GET', url: json, timeout: 5000}).
                // this callback will be called asynchronously
                // when the response is available.
                success(function (d) {
                    data = d;
                    PostsStorage.save(data);
                    deferred.resolve();
                }).
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                error(function () {
                    data = PostsStorage.all();
                    deferred.reject();
                });

            return promise;

        };

        service.getAll = function () {
            return data;
        };

        service.get = function (postId) {
            return data.posts[postId];
        };

        return service;
    })

// ServerPosts Data: JSON Wordpress Posts configuration with Server Side pagination
    .factory('ServerPostsData', function ($http, $q, ServerPostsStorage) {

        var data = [];
        var service = {};

        /* (For DEMO purposes) Local JSON data */
        var json = 'http://www.mondosenzaglutineroma.it/api/get_recent_posts?';

        /* Set your URL as you can see in the following example */
        /* NOTE: In case of the default permalinks, you should add '&' at the end of the url */
        // var json = 'YourWordpressURL/?json=get_recent_posts&';

        /* With user-friendly permalinks configured */
        /* NOTE: In case of the user-friendly permalinks, you should add '?' at the end of the url */
        // var json = 'YourWordpressURL/api/get_recent_posts?';

        service.getURL = function () {
            return json;
        };

        service.setData = function (posts) {
            data = posts;
        };

        service.get = function (serverpostId) {
            return data[serverpostId];
        };

        return service;
    })

// RSS Feeds Data: JSON
    .factory('FeedsData', function ($http, $q, FeedsStorage) {

        var xml = 'http://www.aiclazio.it/attivita-e-notizie/eventi/RSS';
        var url = 'http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=50&callback=JSON_CALLBACK&q=' + encodeURIComponent(xml);

        var deferred = $q.defer();
        var promise = deferred.promise;
        var data = [];
        var service = {};
        var entries = [];

        service.async = function () {
            $http({method: 'JSONP', url: url, timeout: 5000}).
                // this callback will be called asynchronously
                // when the response is available.
                success(function (d) {
                    data = d;
                    FeedsStorage.save(data.responseData.feed);
                    entries = data.responseData.feed.entries;
                    deferred.resolve();
                }).
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                error(function () {
                    data = FeedsStorage.all();
                    entries = data.entries;
                    deferred.reject();
                });

            return promise;

        };

        service.getAll = function () {
            return data.responseData.feed;
        };

        service.get = function (entryId) {
            return entries[entryId];
        };

        return service;
    })

    .factory('$localstorage', ['$window', function ($window) {
        return {
            set: function (key, value) {
                $window.localStorage[key] = value;
            },
            get: function (key, defaultValue) {
                return $window.localStorage[key] || defaultValue;
            },
            setObject: function (key, value) {
                $window.localStorage[key] = JSON.stringify(value);
            },
            getObject: function (key) {
                return JSON.parse($window.localStorage[key] || '{}');
            }
        }
    }])

    .factory('prendiResiduo', function ($http, API_URL) {

        var response = {};

        var addresponse = function (risposta) {
            response = risposta;
        }

        var addcf = function (cf) {
            return $http.get(API_URL + cf);
        }

        var getdati = function () {
            return response;
        }


        return {
            addcf: addcf,
            getdati: getdati,
            addresponse: addresponse
        };

    })
// Settings Data: Settings configuration
    .factory('SettingsData', function () {
        var data = {};

        data.items = {
            options: [
                {
                    name: 'First Option',
                    value: true
                },
                {
                    name: 'Second Option',
                    value: false
                },
                {
                    name: 'Third Option',
                    value: false
                },
                {
                    name: 'Fourth Option',
                    value: false
                },
                {
                    name: 'Fifth Option',
                    value: false
                }],
            sorting: 'A',
            range: 30
        };

        return data;
    })

